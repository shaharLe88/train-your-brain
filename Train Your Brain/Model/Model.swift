//
//  Model.swift
//  Train Your Brain
//
//  Created by admin on 04/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import Foundation

class Model {
    static let instance = Model()
    var data = [Student]()
    
    private init() {
    
        for i in 0...19 {
            let student = Student()
            student.fullName = "Student Name " + String(i)
            student.id = String(Int.random(in: 100000000...999999999))
            student.email = "Student Name@gmail.com"
            addStudent(student: student)
        }
    }
    
    func addStudent(student:Student){
        data.append(student)
    }
    
    func getAllStudent() -> [Student]{
        return data
    }
    
}
