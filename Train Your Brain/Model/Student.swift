//
//  Student.swift
//  Train Your Brain
//
//  Created by admin on 04/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import Foundation

class Student {
    
    var id: String = ""
    var fullName: String = ""
    var email: String = ""
    var profileImageURL: String = ""
    
}
