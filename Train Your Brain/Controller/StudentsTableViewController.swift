//
//  StudentsTableViewController.swift
//  Train Your Brain
//
//  Created by admin on 04/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import UIKit

class StudentsTableViewController: UITableViewController {
    
    var data = [Student]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        data = Model.instance.getAllStudent()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return data.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:StudentViewCell = tableView.dequeueReusableCell(withIdentifier: "StudentCell", for: indexPath) as! StudentViewCell
        
        // Configure the cell...
        let student = data[indexPath.row]
        cell.fullName.text = student.fullName
        cell.fullName.textColor = .link
        cell.id.text = student.id
        cell.email.text = student.email
        
        cell.profileImage.image = UIImage(named: "avatar")
        cell.profileImage.tintColor = .white
        cell.profileImage.layer.masksToBounds = true
        cell.profileImage.layer.cornerRadius = cell.profileImage.frame.size.width / 2.0
        cell.profileImage.layer.backgroundColor = UIColor.black.cgColor
        cell.profileImage.contentMode = .scaleAspectFit
        
        return cell
    }
    
      // According to the student we selected from the list, a new window page opens with the student information
      var studentSelected: Student?
      override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "StudentInfoSegue" {
              let vc:StudentViewController = segue.destination as! StudentViewController
              vc.student = studentSelected
          }
      }
      
      override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          studentSelected = data[indexPath.row]
          performSegue(withIdentifier: "StudentInfoSegue", sender: self)
      }


}
