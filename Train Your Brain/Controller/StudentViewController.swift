//
//  StudentViewController.swift
//  Train Your Brain
//
//  Created by admin on 04/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import UIKit

class StudentViewController: UIViewController {

    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var email: UILabel!
    
    
    var student: Student?
       
       override func viewDidLoad() {
           super.viewDidLoad()

           // Do any additional setup after loading the view.
           
           profileImage.image = UIImage(named: "avatar")
           profileImage.tintColor = .white
           profileImage.layer.masksToBounds = true
           profileImage.layer.cornerRadius = profileImage.frame.size.width / 2.0
           profileImage.layer.backgroundColor = UIColor.black.cgColor
           profileImage.contentMode = .scaleAspectFit
           
           fullName.text = student?.fullName
           fullName.textColor = .link
          
           email.text = student?.email
           email.textColor = .yellow
       }

}
